
const axios = require('axios');
const config = require('../config/config');

module.exports = {
    channelInfo: (channelID) => {
        return axios.get(`https://slack.com/api/channels.info?token=${config.tokens.lambdaTeam}&channel=${channelID}&pretty=1`);
    },
    userInfo: (userID) => {
        return axios.get(`https://slack.com/api/users.info?token=${config.tokens.lambdaTeam}&user=${userID}&pretty=1`);
    },
};