const SlackBot = require('slackbots');

const GameManager = require('./controller/gameManager');
const { tokens } = require('./config/config');

const bot = new SlackBot({
    token: tokens.mafiaBot,
    name: 'MafiaBot'
});

const gameManager = new GameManager();

bot.on('message', async (data) => {
    console.log(data);
    
    if (data.text === 'start') {
        gameManager.newGame(data.channel, bot);
    }
    if (data.text === 'stop') {
        gameManager.gameStop(data.channel, bot);
    }
    if (!data.bot_id) {
        gameManager.addPlayer(data.channel, data.user);
    }     
});