const Game = require('./game');
const { channelInfo } = require('../model/slackapi');

class GameManager {
    constructor() {
        this.games = new Map();       
    }
    
    getGameByChannelID(channelID) {
        return this.games.get(channelID);
    }

    async newGame(channelID, bot) {
        console.log(this.games);
        
        if (!this.games.get(channelID)) {
            const { channel } = (await channelInfo(channelID)).data;
            const game = new Game(channel);

            game.start(bot);
            this.games.set(channelID, game);       
            
            bot.postMessageToChannel(
                channel.name, 
                'game started'
            );
        } else {
            bot.postMessageToChannel(
                (await channelInfo(channelID)).data.channel.name, 
                'Game already exists in this channel'
            );
        }
    }
    
    async gameStop(channelID, bot) {
        const game = this.games.get(channelID);

        if (game) {
            game.stop();

            bot.postMessageToChannel(
                (await channelInfo(channelID)).data.channel.name, 
                'game ended'
            );

            this.games.delete(channelID);   
        } else {
            bot.postMessageToChannel(
                (await channelInfo(channelID)).data.channel.name, 
                'there is no game in this channel'
            );
        }
    }

    addPlayer(channelID, playerID) {
        const game = this.games.get(channelID);

        if (game) {
            game.addPlayerToGame(playerID);
        }
    }
}

module.exports = GameManager;