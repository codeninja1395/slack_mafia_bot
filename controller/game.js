const cron = require('node-cron');
const _ = require('lodash');

const { userInfo } = require('../model/slackapi');

class Game {
    constructor (channel) {      
        this.channel = channel;
        this.checkedPlayers = [];

        this.cron = null;
    }

    stop() {
        this.cron.destroy();
    }

    start(bot) {
        this.cron = cron.schedule('* * * * *', async () => {
            const diff = _.difference(
                this.channel.members, 
                this.checkedPlayers
            );
                    
            bot.postMessageToChannel(
                this.channel.name, 
                `Этой ночью не проснулись:${[...(await this.getMemberNames(diff))]} `
            );

            this.checkedPlayers = []; 
        });
    } 
   
    addPlayerToGame(playerID) {
        if (!this.checkedPlayers.includes(playerID)){
            this.checkedPlayers.push(playerID);
        }          
    }  
    
    async getMemberNames(array) {
        const result = [];
        
        for (let el of array) {
            const user = (await userInfo(el)).data.user;  
            
            if (!user.is_bot) result.push(' ' + user.real_name);
        }

        return result;      
    }      
}

module.exports = Game;